"""BTL URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from home import views
from django.contrib.auth import views as auth_username

admin.autodiscover()

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^dangki.html', views.register, name='register'),
    url(r'^dangkithanhcong.html', views.registerDone, name='registerDone'),
    url(r'^user.html', views.loginRequest, name='loginRequest'),
    url(r'^Reg.html', views.reg, name='reg'),
    url(r'^Upload.html', views.upload, name='upload'),
    url(r'^dangnhap.html', views.login, name='login'),
    url(r'^timkiem.html', views.timkiem, name='timkiem'),
    url(r'^details.html', views.details, name='details'),
    url(r'^$', views.wellcome, name='wellcome'),

]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += staticfiles_urlpatterns()
