from django import forms
import re
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist


class Registertion(forms.ModelForm):
    username = forms.CharField(label="Nhập Mã Sinh Viên", max_length=30)
    email = forms.CharField(label="Nhập Gmail")
    password = forms.CharField(label="Nhập Password", widget=forms.PasswordInput())
    passwordCheck = forms.CharField(label="Nhập Lại Password", widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username',)

    def clean_passwordCheck(self):
        if 'password' in self.cleaned_data:

            password = self.cleaned_data['password']
            passwordCheck = self.cleaned_data['passwordCheck']

            if password == passwordCheck:
                return passwordCheck
            raise forms.ValidationError("Mật khẩu không phù hợp ")

    def clean_userName(self):
        userName = self.cleaned_data['username']
        if not re.search(r'^\w+$', userName):
            raise forms.ValidationError("UserName có ký tự đặc biệt")
        try:
            User.objects.get(username=userName)
        except ObjectDoesNotExist:
            return userName

        raise forms.ValidationError("Tài Khoản đã tồn taị")
