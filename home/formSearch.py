from django import forms
from django.forms import fields


class SearchUET(forms.Form):
    data = forms.CharField(label="Nhập mã lớp hoặc tên môn học cần tìm !", max_length=50)

class Meta:
    fields = ('data',)