import smtplib

from django.contrib.admin import ModelAdmin
from django.contrib.auth.decorators import login_required
from django.core import mail
from django.core.files.storage import FileSystemStorage
from django.core.mail import send_mail
from django.db import IntegrityError
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext

import Import.ReadFile
import Import.Connect
from home.formLogin import Login
from home.formSearch import SearchUET
from .form import *
from home.models import Search, Hk120162017, Hk120152016, Hk220152016, List
from django.contrib.auth import authenticate, get_user_model

diction = {}
data = []
smtp_server = "smtp.gmail.com:587"

def register(request):
    if request.method == 'POST':
        form = Registertion(request.POST)
        print(form.errors)
        if form.is_valid():
            try:
                user = User.objects.create_user(username=form.cleaned_data['username'],
                                                email=form.cleaned_data['email'],
                                                password=form.cleaned_data['password'], )
                return render(request, "html/dangkithanhcong.html", {'new_user': user})
            except IntegrityError as e:
                if 'UNIQUE constraint':
                    user = User.objects.create_user(username=form.cleaned_data['username'],
                                                    email=form.cleaned_data['email'],
                                                    password=form.cleaned_data['password'], )
                    new_user = form.save(commit=False)
                    new_user.save()
                    return render(request, "html/dangkithanhcong.html", {'new_user': form})

        else:
            return render(request, "html/dangki.html", {'form': form})

    form = Registertion()
    return render(request, "html/dangki.html", {'form': form})


def login(request):
    upload()
    return render(request, "html/dangnhap.html")


def wellcome(request):
    upload()
    return render(request, "html/trangchu.html")


def registerDone(request):
    return render(request, "html/dangkithanhcong.html")


def reg(request):
    return render(request, "html/UserExits.html")


def timkiem(request):
    global list, textt
    formSearch = SearchUET(request.POST)
    textt = ""
    print(formSearch.errors)
    if formSearch.is_valid():
        cd = formSearch.cleaned_data
        data = authenticate(data=cd['data'])
        textt = formSearch.cleaned_data['data']
        list = List.showList(formSearch.cleaned_data['data'], "hk120162017")
        dictionList = {
            "dictList": list
        }
        # print(list)
        formSearch.cleaned_data['data'] = None
        return render(request, "html/timkiem.html", dictionList)

    return render(request, 'html/timkiem.html')


def details(request):
    listt = List.showDetails(list[0].get('data'), "hk120162017")
    dictionList = {
        "List": listt
    }
    return render(request, "html/details.html", dictionList)


def loginRequest(request):
    upload()
    global data
    formLogin = Login(request.POST)

    if formLogin.is_valid():

        try:
            cd = formLogin.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if cd['username'] == "admin" and cd['password'] == "admin@123":
                return redirect('http://127.0.0.1:8000/admin/')
            data = Search.find(formLogin.cleaned_data['username'], "hk120162017")
            request.session['username'] = cd['username']
            request.session.set_expiry(15)
            diction = {
                "dict": data
            }

            print(data)
            if user is not None:
                return render(request, "html/user.html", diction)
            else:
                return render(request, "html/dangki.html")


        except IntegrityError as e:
            if 'UNIQUE constraint':
                cd = formLogin.cleaned_data
                user = authenticate(username=cd['username'],
                                    password=cd['password'])
                data = Search.find(formLogin.cleaned_data['username'], "hk120162017")
                request.session['username'] = cd['username']
                request.session.set_expiry(15)
                diction = {
                    "dict": data
                }
                print(data)

                if user is not None:
                    return render(request, "html/user.html", diction)
                else:
                    return render(request, "html/dangki.html")

    else:
        formLogin = Login()
    return render(request, "html/dangnhap.html", {'formLogin': formLogin})


def formView(request):
    if request.session.has_key('username'):
        username = request.session['username']
        return render(request, 'html/dangnhap.html', {'username': username})
    else:
        return render(request, 'html/dangnhap.html', {})


def upload():
    global data

    if (Hk120162017.objects.all().count() > 0) or (Hk120152016.objects.all().count() > 0) \
            or (Hk220152016.objects.all().count() > 0):
        connection = mail.get_connection()
        # Manually open the connection
        connection.open()
        server = smtplib.SMTP(smtp_server)
        server.starttls()
        server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        for i in User.objects.all():
            send_mail('Xem Điểm UET với tốc độ ánh sáng',
                      'Hệ thống đã cập nhật điểm của các môn học mời các bạn vào xem!',
                      settings.EMAIL_HOST_USER,
                      [i.email], fail_silently=False)
        connection.close()
        server.quit()

    if Hk120162017.objects.all() is not None:
        data = {
            'Post': Hk120162017.objects.all()
        }
        for post in Hk120162017.objects.all():
            Import.ReadFile.loading(post.file.path)
            Import.Connect.connect(post.file.path, "Hk120162017")
            print("DONE")
            post.delete()
        Hk120162017.objects.all().delete()

    if Hk120152016.objects.all() is not None:
        data = {
            'Post': Hk120152016.objects.all()
        }
        for post in Hk120152016.objects.all():
            Import.ReadFile.loading(post.file.path)
            Import.Connect.connect(post.file.path, "Hk120152016")
            print("DONE")
            post.delete()
        Hk120152016.objects.all().delete()

    if Hk220152016.objects.all() is not None:
        data = {
            'Post': Hk220152016.objects.all()
        }
        for post in Hk220152016.objects.all():
            Import.ReadFile.loading(post.file.path)
            Import.Connect.connect(post.file.path, "Hk220152016")
            print("DONE")
            post.delete()
        Hk220152016.objects.all().delete()
