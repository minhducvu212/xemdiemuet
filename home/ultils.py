from django.contrib import auth
from django.contrib.auth.models import User


class AutomaticLoginUserMiddleware(object):
    def process_request(self, request):
        user = auth.authenticate(username='admin', password='admin@123')
        if user:
            request.user = user
            auth.login(request, user)
