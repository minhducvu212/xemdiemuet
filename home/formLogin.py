from django import forms
import re
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist


class Login(forms.Form):
    username = forms.CharField(label="Nhập Mã Sinh Viên", max_length=30)
    password = forms.CharField(label="Nhập Password", widget=forms.PasswordInput())


