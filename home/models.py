import uuid

import MySQLdb
import os
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db import models

# Create your models here.
diction = {}
data = []
dictionList = {}
dataList = []
dictionListt = {}
dataListt = []
dictionListDetails = {}
dataListtDetails = []
dictionLop = {}
dataLop = []

class Search():
    def find(msv, database):
        del data[:]
        diction.clear()

        db = MySQLdb.connect(host="localhost", user="root", passwd="minhducvu@123", db="CSDL_DiemThi", charset="utf8",
                             use_unicode=True)
        cursor = db.cursor()
        query = (
            "SELECT m_msv, m_hoten, m_ngaysinh, m_lop, m_mon, m_diemTP, m_diemCK, m_diemTK FROM " + database + " WHERE m_msv = " + msv)
        cursor.execute(query)
        print("Mã Sinh Viên", "\t", " Họ Và Tên", "\t", " Ngày Sinh", "\t", "\t", "Lớp", "\t", "\t", "\t", "\t",
              "Môn Học", "\t", "\t", "\t", "\t", "\t", "Điểm Giữa Kì", "\t", "Điểm Cuối Kì", "\t", "Điểm Tổng Kết",
              "\n")
        for (msv, m_hoten, m_ngaysinh, m_lop, m_mon, m_diemTP, m_diemCK, m_diemTK) in cursor:
            # print(" ", msv[0:msv.index(".")], "\t", "\t", m_hoten, "\t", m_ngaysinh, "\t", m_lop,
            #       "\t", m_mon, "\t", "\t", "\t", "\t", "\t", "\t", m_diemTP, "\t", "\t",
            #       "\t", m_diemCK, "\t", "\t", "\t", m_diemTK)

            diction['msv'] = msv[0:msv.index(".")]
            diction['hoten'] = m_hoten
            diction['ngaysinh'] = m_ngaysinh
            diction['lop'] = m_lop
            diction['mon'] = m_mon
            diction['diemTP'] = m_diemTP
            diction['diemCK'] = m_diemCK
            diction['diemTK'] = m_diemTK
            result = dict(diction, **diction)
            data.append(result)
            print(diction)

        cursor.close()

        db.close()

        return data


image_storage = FileSystemStorage(
    # Physical file location ROOT
    location=u'{0}/my_sell/'.format(settings.MEDIA_ROOT),
    # Url for file
    base_url=u'{0}my_sell/'.format(settings.MEDIA_URL),
)


class List():
    def showList(text, database):
        del dataList[:]
        dictionList.clear()
        dataList.clear()
        dictionLop.clear()
        dataLop.clear()

        db = MySQLdb.connect(host="localhost", user="root", passwd="minhducvu@123", db="CSDL_DiemThi", charset="utf8",
                             use_unicode=True)
        cursor = db.cursor()
        query = "SELECT m_malop, m_data FROM " + database + " WHERE LOCATE('" + text + "', m_data) > 0"
        # print(query)
        cursor.execute(query)

        dictionLop['malop'] = cursor._rows[0][0]
        result1 = dict(dictionLop, **dictionLop)
        dataLop.append(result1)
        print(dictionLop)

        for j in range(len(cursor._rows)):
            m_data = cursor._rows[j]
            for i in range(len(cursor._rows)):
                dictionList['data'] = m_data[1]
                result = dict(dictionList, **dictionList)
                dataList.append(result)
                break
                # print(dictionList)
            print(dictionList)

        cursor.close()

        db.close()

        return dataList

    def showDetails(text, database):
        del dataList[:]
        dictionList.clear()
        dataList.clear()
        dictionListDetails.clear()
        dataListtDetails.clear()

        db = MySQLdb.connect(host="localhost", user="root", passwd="minhducvu@123", db="CSDL_DiemThi", charset="utf8",
                             use_unicode=True)
        cursor = db.cursor()



        query1 = (
            "SELECT m_msv, m_hoten, m_lop, m_mon, m_diemTP, m_diemCK, m_diemTK FROM " + database + " WHERE m_malop = " + "'" +
            dictionLop['malop'] + "'")
        cursor.execute(query1)
        print("Mã Sinh Viên", "\t", " Họ Và Tên", "\t", " Ngày Sinh", "\t", "\t", "Lớp", "\t", "\t", "\t", "\t",
              "Môn Học", "\t", "\t", "\t", "\t", "\t", "Điểm Giữa Kì", "\t", "Điểm Cuối Kì", "\t", "Điểm Tổng Kết",
              "\n")
        for (m_msv, m_hoten, m_lop, m_mon, m_diemTP, m_diemCK, m_diemTK) in cursor:
            # print(" ", msv[0:msv.index(".")], "\t", "\t", m_hoten, "\t", m_ngaysinh, "\t", m_lop,
            #       "\t", m_mon, "\t", "\t", "\t", "\t", "\t", "\t", m_diemTP, "\t", "\t",
            #       "\t", m_diemCK, "\t", "\t", "\t", m_diemTK)

            dictionListDetails['msv'] = m_msv[0:m_msv.index(".")]
            dictionListDetails['hoten'] = m_hoten
            # dictionListDetails['ngaysinh'] = m_ngaysinh
            dictionListDetails['lop'] = m_lop
            dictionListDetails['mon'] = m_mon
            dictionListDetails['diemTP'] = m_diemTP
            dictionListDetails['diemCK'] = m_diemCK
            dictionListDetails['diemTK'] = m_diemTK
            result = dict(dictionListDetails, **dictionListDetails)
            dataListtDetails.append(result)
            print(dictionListDetails)

        cursor.close()

        db.close()

        return dataListtDetails


class Hk120162017(models.Model):
    tittle = models.CharField(max_length=200)
    body = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now_add=True)
    file = models.FileField(null=False)

    def __str__(self):
        return self.tittle

    @property
    def filename(self):
        return self.file.name.rsplit('/', 1)[-1]


class Hk120152016(models.Model):
    tittle = models.CharField(max_length=200)
    body = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now_add=True)
    file = models.FileField(null=False)

    def __str__(self):
        return self.tittle


class Hk220152016(models.Model):
    tittle = models.CharField(max_length=200)
    body = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now_add=True)
    file = models.FileField(null=False)

    def __str__(self):
        return self.tittle


def file_link(self):
    return self.file.url
