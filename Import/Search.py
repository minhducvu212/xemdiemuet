import string

import MySQLdb


def find(msv):
    db = MySQLdb.connect(host="localhost", user="root", passwd="minhducvu@123", db="CSDL_DiemThi", charset="utf8",
                         use_unicode=True)

    cursor = db.cursor()
    query = ("SELECT m_msv, m_hoten, m_ngaysinh, m_lop, m_mon, m_diemTP, m_diemCK, m_diemTK FROM hk220162017 WHERE m_msv = " + msv )
    cursor.execute(query)
    print("Mã Sinh Viên", "\t", " Họ Và Tên", "\t", " Ngày Sinh", "\t","\t", "Lớp", "\t","\t","\t","\t",
          "Môn Học","\t","\t","\t","\t","\t","Điểm Giữa Kì","\t", "Điểm Cuối Kì", "\t", "Điểm Tổng Kết", "\n")
    for ( msv,m_hoten, m_ngaysinh, m_lop,m_mon, m_diemTP, m_diemCK, m_diemTK) in cursor:
        print(" ", msv[0:msv.index(".")], "\t","\t", m_hoten, "\t", m_ngaysinh,  "\t",m_lop,
              "\t",m_mon,"\t","\t","\t","\t", "\t","\t",m_diemTP,"\t","\t",
              "\t",m_diemCK,"\t","\t", "\t",m_diemTK)

    cursor.close()

    db.close()