import MySQLdb
import xlrd


def connect(name, databaseName):
    db = MySQLdb.connect(host="localhost", user="root", passwd="minhducvu@123", db="CSDL_DiemThi", charset="utf8",
                         use_unicode=True)

    cursor = db.cursor()

    cursor.execute("SELECT VERSION()")

    data = cursor.fetchone()

    print("Database version : %s " % data)

    # import
    book = xlrd.open_workbook(name)
    sheet = book.sheet_by_name("Sheet1")

    # cursor.execute("DROP TABLE IF EXISTS " + databaseName)
    # sql = """ CREATE TABLE IF NOT EXISTS HK220152016   (
    #                                         i_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    #                                         m_msv TEXT NOT NULL,
    #                                         m_hoten TEXT NOT NULL,
    #                                         m_ngaysinh TEXT NOT NULL ,
    #                                         m_lop TEXT NOT NULL ,
    #                                         m_diemTP TEXT ,
    #                                         m_diemCK TEXT ,
    #                                         m_diemTK TEXT NOT NULL,
    #                                         m_mon TEXT NOT NULL,
    #                                         m_malop TEXT NOT NULL,
    #                                         m_data TEXT NULL
    #                                     ); """
    # cursor.execute(sql)

    print("==========================")

    for r in range(sheet.nrows):

        sql1 = "INSERT INTO " + databaseName + "(m_msv, m_hoten, m_ngaysinh, m_lop, m_diemTP,m_diemCK, m_diemTK, m_mon, m_malop, m_data)" \
               "VALUES ('%s','%s' ,'%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % \
               (sheet.cell(r, 1).value, sheet.cell(r, 2).value, sheet.cell(r, 3).value, sheet.cell(r, 4).value,
                sheet.cell(r, 5).value, sheet.cell(r, 6).value, sheet.cell(r, 7).value, sheet.cell(r, 8).value,
                sheet.cell(r, 9).value, sheet.cell(r, 10).value)
        try:
            cursor.execute(sql1)
            print()
            db.commit()
        except:
            print(db.error())
            db.rollback()

    db.close()
